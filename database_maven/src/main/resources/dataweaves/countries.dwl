%dw 1.0
%output application/json skipNullOn = "everywhere"
%type currency = :string { format: "###,##.00"}
%type age = :string {format: "##"}
%type numformat = :string {format: "#.#"}
---
payload map ((payload01 , indexOfPayload01) -> {
	CountryCode2: payload01.Code2,
	IndependenceYear: payload01.IndepYear as :number when payload.IndepYear !=null otherwise null,
	GNPOld: payload01.GNPOld as :currency when payload.GNPOLD !=null otherwise null,
	Region: payload01.Region,
	countryCapital: payload01.Capital,
	HeadOfState: payload01.HeadOfState,
	countryCode: lower payload01.Code,
	Continent: payload01.Continent replace /(payload.Continent)/ with "CountryContinent",
	SurfaceArea: payload01.SurfaceArea as :number,
	LocalName: payload01.LocalName,
	GovernmentBy: payload01.GovernmentForm,
	countryPopulation: payload01.Population,
	LifeExpectancy: payload01.LifeExpectancy as :age,
	countryName: upper payload01.Name  ++ '' ++ 'THE GREAT',
	GNP: payload01.GNP as :currency,
	languages: flowVars.countries map ((country , indexOfCountry) -> {
		CountryCode: country.CountryCode, 
		IsOfficial: country.IsOfficial, 
		Language: country.Language,
		Percentage: country.Percentage as :numformat
	}-"CountryCode" )
}) 
